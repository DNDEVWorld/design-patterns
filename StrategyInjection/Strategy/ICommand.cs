﻿namespace StrategyInjection
{

    [AutoRegister(true)]
    public interface ICommand
    {
        CommandNameEnum Name { get;}

        void Execute();
    }
}