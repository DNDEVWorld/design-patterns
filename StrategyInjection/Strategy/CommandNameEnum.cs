﻿namespace StrategyInjection
{
    public enum CommandNameEnum
    {
        ExportCommand, 
        ImportCommand
    }
}
