﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity;

namespace StrategyInjection
{
    public class CommandFactory : ICommandFactory
    {
        private IEnumerable<ICommand> _commands;

        public CommandFactory(IEnumerable<ICommand> commands)
        {
            this._commands = commands;
        }     

        public ICommand Create(CommandNameEnum commandName) 
        {
            return _commands.FirstOrDefault(c => c.Name == commandName);
        }
    }
}
