﻿namespace StrategyInjection
{
    [AutoRegister]
    public interface ICommandFactory
    {
        ICommand Create(CommandNameEnum commandName);
    }
}