﻿using System;

namespace StrategyInjection
{
    public class ImportCommand : ICommand
    {
        public CommandNameEnum Name => CommandNameEnum.ImportCommand;
        
        public void Execute()
        {
            Console.WriteLine("Importation");
        }
    }
}
