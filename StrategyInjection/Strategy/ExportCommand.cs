﻿using System;

namespace StrategyInjection
{
    public class ExportCommand : ICommand 
    {
        public CommandNameEnum Name => CommandNameEnum.ExportCommand;
      

        public void Execute()
        {
            Console.WriteLine("Exportation");
        }
    }
}
