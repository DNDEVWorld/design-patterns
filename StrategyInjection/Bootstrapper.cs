﻿using System;
using System.Linq;
using System.Reflection;
using Unity;

namespace StrategyInjection
{
    public class Bootstrapper
    {
        public IUnityContainer UnityContainer { get; private set; } = new UnityContainer();
        public void Run()
        {
            ConfigContainer();
        }

        private void ConfigContainer()
        {
            var types = Assembly.GetExecutingAssembly().GetExportedTypes();
            foreach (Type i in types.Where(t => t.GetCustomAttributes<AutoRegisterAttribute>().Any()))
            {
                var attribute = i.GetCustomAttribute<AutoRegisterAttribute>();
                foreach (Type t in types.Where(t => i.IsAssignableFrom(t) && !t.IsInterface))
                {
                    if (attribute.UseTypeForName)
                        UnityContainer.RegisterType(i, t, t.Name);
                    else
                        UnityContainer.RegisterType(i, t);
                }

            }

            
        }
    }
}
