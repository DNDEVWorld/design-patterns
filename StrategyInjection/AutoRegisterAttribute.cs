﻿using System;

namespace StrategyInjection
{
    public class AutoRegisterAttribute : Attribute
    {
        public bool UseTypeForName { get; }

        public AutoRegisterAttribute(bool useTypeForName = false)
        {
            UseTypeForName = useTypeForName;
        }
    }
}
