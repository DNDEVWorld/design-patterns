﻿using StrategyInjection;
using System;
using System.Linq;
using System.Reflection;
using Unity;

namespace StrategyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            Bootstrapper bootstrapper = new Bootstrapper();
            bootstrapper.Run();
            var commandFactorty = bootstrapper.UnityContainer.Resolve<ICommandFactory>();
            var command = commandFactorty.Create(CommandNameEnum.ExportCommand);

            command.Execute();

            Console.ReadKey();
        }

        
    }
}
