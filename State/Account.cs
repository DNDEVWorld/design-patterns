﻿using System;
using State.AccountStates;

namespace State
{
    public class Account
    {
        public Account(Action onUnfreeze)
        {
            State = new NotVerified(onUnfreeze);
        }

        public double Balance { get; private set; }

        public IAccountState State { get; private set; } 

        public void Deposit(double amount)
        {
            State = State.Deposite(() => Balance += amount);
        }

        public void Withdraw(double amount)
        {
            State = State.Withdraw(() => Balance -= amount);
        }

        public void Close()
        {
            State = State.Close();
        }

        public void VerifyHolder()
        {
           State = State.VerifyHolder();
        }

        public void Freeze()
        {
            State = State.Freeze();
        }
    }
}
