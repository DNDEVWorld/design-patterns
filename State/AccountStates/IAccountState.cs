﻿using System;

namespace State.AccountStates
{
    public interface IAccountState
    {
        IAccountState Deposite(Action addToBalance);

        IAccountState Withdraw(Action subtractFromBalance);

        IAccountState Close();

        IAccountState VerifyHolder();

        IAccountState Freeze();
    }
}
