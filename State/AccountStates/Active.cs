﻿using System;

namespace State.AccountStates
{
    public class Active : IAccountState
    {
        private Action OnUnFreeze { get; }

        public Active(Action onUnfreeze)
        {
            OnUnFreeze = onUnfreeze;
        }

        public IAccountState Deposite(Action addToBalance)
        {
            addToBalance();
            return this;
        }

        public IAccountState Withdraw(Action subtractFromBalance)
        {
            subtractFromBalance();
            return this;
        }
        public IAccountState Close() => this;
        public IAccountState VerifyHolder() => this;
        

        public IAccountState Freeze()
        {
            return new Frozen(OnUnFreeze);
        }
    }
}
