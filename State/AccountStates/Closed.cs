﻿using System;

namespace State.AccountStates
{
    public class Closed : IAccountState
    {
        public IAccountState Deposite(Action addToBalance) => this;

        public IAccountState Withdraw(Action subtractFromBalance) => this;

        public IAccountState Close() => this;

        public IAccountState VerifyHolder() => this;

        public IAccountState Freeze() => this;
    }
}
