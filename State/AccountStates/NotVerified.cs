﻿using System;

namespace State.AccountStates
{
    public class NotVerified : IAccountState
    {
        private readonly Action _onUnfreeze;
        public NotVerified(Action onUnfreeze)
        {
            _onUnfreeze = onUnfreeze;
        }

        public IAccountState Deposite(Action addToBalance)
        {
            addToBalance();
            return this;
        }

        public IAccountState Withdraw(Action subtractFromBalance) => this;

        public IAccountState Close() => new Closed();

        public IAccountState VerifyHolder()
        {
            return new Active(_onUnfreeze);
        }

        public IAccountState Freeze() => this;
    }
}
