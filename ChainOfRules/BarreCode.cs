﻿using System;
using ChainOfRules.Rules;

namespace ChainOfRules
{
    public class BarreCode
    {
        public BarreCode(
            Action processSupportAction, 
            Action processWorkOrderAction, 
            Action processStorageContainerAction,
            IBarreCodeProcessorFactory rulesFactory)
        {
            BarreCodeProcessorRule = rulesFactory.Create(processSupportAction, processWorkOrderAction,
                processStorageContainerAction);
        }

        public IBarreCodeProcessorRules BarreCodeProcessorRule { get; private set; }

        public void ProcessSupport()
        {
            BarreCodeProcessorRule.ProcessSupport();
        }

        public void ProcessWorkOrder()
        {
            BarreCodeProcessorRule.ProcessWorkOrder();
        }

        public void ProcessStorageContainer()
        {
            BarreCodeProcessorRule.ProcessStorageContainer();
        }
        
    }
}
