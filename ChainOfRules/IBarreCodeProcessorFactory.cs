﻿using System;
using ChainOfRules.Rules;

namespace ChainOfRules
{
    public interface IBarreCodeProcessorFactory
    {
        IBarreCodeProcessorRules Create(Action processSupportAction, Action processWorkOrderAction, Action processStorageContainerAction);
    }
}