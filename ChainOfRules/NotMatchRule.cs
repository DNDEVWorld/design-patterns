﻿using System;
using ChainOfRules.Rules;

namespace ChainOfRules
{
    public class NotMatchRule : IBarreCodeProcessorRules
    {
        public void ProcessSupport() { }

        public void ProcessWorkOrder() { }

        public void ProcessStorageContainer() { }

        public Action Process => () => Console.WriteLine("No barre code matches");
    }
}