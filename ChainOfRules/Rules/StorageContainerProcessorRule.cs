﻿using System;

namespace ChainOfRules.Rules
{
    public class StorageContainerProcessorRule : BarreCodeChainedRule
    {
        private readonly Action _processAction;

        public StorageContainerProcessorRule(Action processAction, IBarreCodeProcessorRules next) : base(next)
        {
            _processAction = processAction;
        }

        protected override void HandleStorageContainerProccess()
        {
            _processAction();
        }
    }
}