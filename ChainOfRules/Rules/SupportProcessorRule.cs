﻿using System;

namespace ChainOfRules.Rules
{
    public class SupportProcessorRule : BarreCodeChainedRule
    {
        private Action ProcessAction { get; }

        public SupportProcessorRule(Action processAction, IBarreCodeProcessorRules next) : base(next)
        {
            ProcessAction = processAction;
        }

        protected override void HandleSupportProccess()
        {
            ProcessAction();
        }
    }
}