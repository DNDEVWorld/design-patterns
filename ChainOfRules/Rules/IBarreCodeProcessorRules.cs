﻿using System;

namespace ChainOfRules.Rules
{
    public interface IBarreCodeProcessorRules
    {
        void ProcessSupport();

        void ProcessWorkOrder();

        void ProcessStorageContainer();

        Action Process { get; }
    }
}