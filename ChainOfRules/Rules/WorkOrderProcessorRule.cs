﻿using System;

namespace ChainOfRules.Rules
{
    public class WorkOrderProcessorRule : BarreCodeChainedRule
    {
        public Action ProcessAction { get; }

        public WorkOrderProcessorRule(Action processAction, IBarreCodeProcessorRules next) : base(next)
        {
            ProcessAction = processAction;
        }

        protected override void HandleWorkOrderProccess()
        {
            ProcessAction();
        }
    }
}