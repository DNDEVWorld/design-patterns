﻿using System;

namespace ChainOfRules.Rules
{
    public class BarreCodeChainedRule : IBarreCodeProcessorRules
    {        

        public BarreCodeChainedRule(IBarreCodeProcessorRules next)
        {
            _next = next;         
        }

        private IBarreCodeProcessorRules _next;

       public Action Process { get; protected set; }

        public void ProcessSupport()
        {
            HandleSupportProccess();
            _next.ProcessSupport();
        }

        public void ProcessWorkOrder()
        {
            HandleWorkOrderProccess();
            _next.ProcessWorkOrder();
        }

        public void ProcessStorageContainer()
        {
            HandleStorageContainerProccess();
            _next.ProcessStorageContainer();
        }

        protected virtual void HandleSupportProccess() { }
        protected virtual void HandleWorkOrderProccess() { }
        protected virtual void HandleStorageContainerProccess() { }
    }
}