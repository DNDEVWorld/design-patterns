﻿using System;
using ChainOfRules.Rules;

namespace ChainOfRules
{
    public class DefaultRules : IBarreCodeProcessorFactory
    {
        public IBarreCodeProcessorRules Create(Action processSupportAction, Action processWorkOrderAction,
            Action processStorageContainerAction) => 
                new SupportProcessorRule(processSupportAction, 
                    new WorkOrderProcessorRule(processWorkOrderAction, 
                        new StorageContainerProcessorRule(processStorageContainerAction, 
                           new NotMatchRule())));
       
    }
}