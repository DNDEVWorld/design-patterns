﻿using System;
using ChainOfRules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChainOfRulesTest
{
    [TestClass]
    public class BarreCodeTest
    {
        [TestMethod]
        public void SupportBarreCodeProcess()
        {
            string result = string.Empty;
            var barreCode = new BarreCode(
                () => { result = "Process of support";},
                () => { result = "Process of workOrder";},
                () => { result = "Process of StorageContainer";},
                new DefaultRules()
            );

            barreCode.ProcessWorkOrder();

            Assert.AreEqual(result, "Process of workOrder");


        }
    }
}
