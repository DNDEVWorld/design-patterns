﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using State;
using State.AccountStates;

namespace StateTest
{
    [TestClass]
    public class AccountStateTest
    {
        [TestMethod]
        public void VerifyHolderChangeAccountStateToActive()
        {
            var account = new Account(() => { });
            account.VerifyHolder();

            Assert.IsTrue(account.State is Active);
        }

        [TestMethod]
        public void WithdrawBeforAndAfrterVerifyHolderAccount()
        {
            var account = new Account(() => { });
            account.Deposit(10);
            account.Withdraw(5);
            account.VerifyHolder();
            account.Withdraw(5);

            Assert.AreEqual(5, account.Balance);
        }
      
    }
}
